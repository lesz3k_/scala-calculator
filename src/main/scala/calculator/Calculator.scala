package calculator

import scala.collection.immutable.Range

sealed abstract class Expr
final case class Literal(v: Double) extends Expr
final case class Ref(name: String) extends Expr
final case class Plus(a: Expr, b: Expr) extends Expr
final case class Minus(a: Expr, b: Expr) extends Expr
final case class Times(a: Expr, b: Expr) extends Expr
final case class Divide(a: Expr, b: Expr) extends Expr

object Calculator {
  def computeValues(namedExpressions: Map[String, Signal[Expr]]): Map[String, Signal[Double]] = {
    for(n <- namedExpressions) yield (n._1 -> Signal(eval(Set(), n._2(), namedExpressions)))
  }

  def eval(resolvedRefs: Set[Ref], expr: Expr, references: Map[String, Signal[Expr]]): Double = {
    expr match {
      case Literal(v) => v
      case Plus(a, b) => eval(resolvedRefs, a, references) + eval(resolvedRefs, b, references)
      case Minus(a, b) => eval(resolvedRefs, a, references) - eval(resolvedRefs, b, references)
      case Times(a, b) => eval(resolvedRefs, a, references) * eval(resolvedRefs, b, references)
      case Divide(a, b) => eval(resolvedRefs, a, references) / eval(resolvedRefs, b, references)
      case Ref(v) => {
        if(resolvedRefs.contains(Ref(v))) Double.NaN
        else eval(resolvedRefs + Ref(v), getReferenceExpr(v, references), references)
      }
      case _ => Double.NaN
    }
  }

  /** Get the Expr for a referenced variables.
    *  If the variable is not known, returns a literal NaN.
    */
  private def getReferenceExpr(name: String,
                               references: Map[String, Signal[Expr]]) = {
    references.get(name).fold[Expr] {
      Literal(Double.NaN)
    } { exprSignal =>
      exprSignal()
    }
  }
}
