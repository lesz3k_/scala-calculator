package calculator

object Polynomial {
  def computeDelta(a: Signal[Double], b: Signal[Double],
      c: Signal[Double]): Signal[Double] = {
    Signal(Math.pow(b(), 2) - 4 * c() * a())
  }

  def computeSolutions(a: Signal[Double], b: Signal[Double],
      c: Signal[Double], delta: Signal[Double]): Signal[Set[Double]] = {
    Signal {
      delta() match {
        case d if d < 0 => Set(0)
        case d if d == 0 => Set(-b() / 2 * a())
        case d if d > 0 => Set(((-b() + d) / 2 * a()), ((-b() - d)/ 2 * a()))
      }
    }
  }
}
